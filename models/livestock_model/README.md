# Livestock Model

The Livestock model uses gridded climate and soils data to quantify the productivity of a rangeland area to support livestock.
The model estimate spatially disaggregated livestock densities at a monthly timestep by comparing modeled forage productivity in 
the absence of grazing to observed vegetation detected by the remotely sensed index.
With this estimated livestock density and climate inputs, the model predicts the combined impacts of livestock management 
and climate in controlling rangeland condition and livestock diet sufficiency.

## Input datasets

The input datasets include:
* Bulk density (20 cm depth)
* pH (20 cm depth)
* Sand content (20 cm depth)
* Silt content (20 cm depth)
* Clay content (20 cm depth)
* Monthly average minimum daily temperature
* Monthly average maximum daily temperature
* Monthly precipitation
* Monthly vegetation index
* Plant functional type fractional cover
* Animal grazing areas
* Initial conditions

## The model
The Century model (version 4.6, Parton ​ et al.1988) is used to predict forage growth from climate and soils data,
including the impacts of grazing.
Coupled with the forage production model, the herbivore diet selection and physiology submodel simulates diet selection 
and estimates whether the selected diet meets or exceeds maintenance energy needs.
The herbivore diet and physiology submodels are adapted from GRAZPLAN, which was developed for ruminant livestock (Freer ​ et al. ​ 2012).

## Output of the model
Livestock model output include:
* Potential Biomass (kg/ha). This is total potential modeled biomass in the absence of grazing, including live and standing dead fractions of all plant functional types. `luigi --module models.livestock_model.tasks models.livestock_model.tasks.PotentialBiomass --time 2015-01-01-2016-01-01 --local-scheduler --geography /usr/src/app/models/geography/boundaries/ethiopia_2d.geojson --workers 20 --country-level Ethiopia`

* Standing biomass (kg/ha). This is total modeled biomass after offtake by grazing animals, including live and standing dead fractions of all plant functional types. `luigi --module models.livestock_model.tasks models.livestock_model.tasks.StandingBiomass --time 2015-01-01-2016-01-01 --local-scheduler --geography /usr/src/app/models/geography/boundaries/ethiopia_2d.geojson --workers 20 --country-level Ethiopia`

* Animal Density. Distribution of animals inside grazing area polygons. `luigi --module models.livestock_model.tasks models.livestock_model.tasks.AnimalDensity --time 2015-01-01-2016-01-01 --local-scheduler --geography /usr/src/app/models/geography/boundaries/ethiopia_2d.geojson --workers 20 --country-level Ethiopia`

* Diet sufficiency. Ratio of metabolizable energy intake to maintenance energy requirements on pixels where animals grazed. `luigi --module models.livestock_model.tasks models.livestock_model.tasks.DietSufficiency --time 2015-01-01-2016-01-01 --local-scheduler --geography /usr/src/app/models/geography/boundaries/ethiopia_2d.geojson --workers 20 --country-level Ethiopia`