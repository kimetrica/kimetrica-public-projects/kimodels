# Crop Model Overview
Crop-model runs gridded crop simulation models using the Decision Support System for Agrotechnology Transfer ([DSSAT](https://dssat.net/)) in a docker container. The DSSAT system is configured to be run in a gridded mode at a horizontal grid spacing of approximately 10 km. A reference grid assigned a unique ID (Identification Number) for each grid cell was developed and is used to defines the location and soil type for each grid.

The DSSAT models require daily weather data, soil type, and soil profile properties, as well as cultivar-specific inputs and management including irrigation and amount and type of fertilizer among others for simulating growth, development, and yield. The detailed gridded crop modeling results for each grid cell are aggregated using an area-weighted averaging method according [crop specific production](https://doi.org/10.7910/DVN/PRFF8V) technologies namely: irrigated, rainfed and high/low-input and rainfed subsistence. Finally, the results are further aggregated by second level administrative units of the country.

## Input Data
In order to run a crop model the following data are required:
- **Site weather data**
    - Latitude and longitude of the weather station*,
    - Daily values of incoming solar radiation (MJ/m²-day)*,
    - Maximum and minimum daily air temperature (ºC)*,
    - Daily total rainfall (mm)*,
    - Daily relative humidity (%),
    - Daily wind speed (km-day), and
    - Daily dew point temperature (°C). 

The weather input data used, covering the period 1983 to June 2020, was collated from various sources including [CHIRPS](https://www.chc.ucsb.edu/data/chirps), [AgERA5](https://cds.climate.copernicus.eu/cdsapp#!/dataset/sis-agrometeorological-indicators) and [ERA5](https://cds.climate.copernicus.eu/cdsapp#!/dataset/reanalysis-era5-land). Variable with asterisk (*) above represent minimum required weather data.
- **Soil surface characteristics and soil profile data**
    - Desired soil surface data include: soil classification according to [NRCS](https://www.nrcs.usda.gov/wps/portal/nrcs/main/soils/survey/class/), surface slope, color (Munsell color system), permeability, and drainage class.
    - Soil profile data by soil horizons include: upper and lower horizon depths (cm), percentage sand, silt, and clay content, 1/3 bar bulk density, organic carbon, pH in water, aluminum saturation, and root abundance information.

The soil data used by cro-model was downloaded from [Global High-Resolution Soil Profile Database for Crop Modeling Applications](https://doi.org/10.7910/DVN/1PEEY0).
- **Crop genetic information**
    - Ecotype information for cereal crops include: base temperature below which development occurs (°C), temperature at which maximum development rate occurs during vegetative stage (°C), temperature at which maximum development rate occurs for reproductive stage (°C), radiation use efficiency, g plant dry matter (MJ PAR−¹), canopy light extinction coefficient for daily PAR, etc.
    - Cultivar information for cereal crops include: identification code or number for a specific cultivar, name of cultivar, Number of experiments used to estimate cultivar parameters, ecotype code or this cultivar, points to the Ecotype in the ECO file, degree days (base 8°C) from emergence to end of juvenile phase, photoperiod sensitivity coefficient, degree days (base 8°C) from silking to physiological maturity, potential kernel number, potential kernel growth rate, degree days required for a leaf tip to emerge (phyllochron interval), etc.

Crop genetic information used in the crop-model are obtained from DSSAT genetic coefficients database with additional ones  obtained from [literature review](https://docs.google.com/spreadsheets/d/1vIWCLO91jEx1SCwCG9G3Ve2TIFNs4ZOl5_aQxtCvExI/edit?usp=sharing).
- **Crop management**
    - Planting details, irrigation and water management, fertilization, residue applications and harvest details. 

The crop management practices information are obtained from DSSAT database and literature review.

- **Crop specific harvested area**
    - Cereals and pulses crops harvested area under irrigated portion of crop, rain-fed high inputs portion of crop, rain-fed low inputs portion of crop and rain-fed subsistence portion of crop.

Crop specific harvested area data used in the crop-model was obtained from [Global Spatially-Disaggregated Crop Production Statistics Data for 2010 Version 2.0](https://doi.org/10.7910/DVN/PRFF8V).

- **Reference grid**
    - A reference grid assigned a unique ID to  defines the location and soil type for each grid.

The reference grid was developed by kimetrica for the whole Africa.

The input data described above for running crop-model covers Ethiopia and South Sudan, and saved in CKAN.

The source of the data used to run DSSAT in South Sudan and Ethiopia has been prepared by University of Florida and Kimetrica and saved in CKAN.

## Quickstart code
To run a Luigi task in a local environment, make sure the PYTHONPATH has been set to the right directories. It's recommended to run this inside Docker container built with Kimetrica's pre-defined requirements and dependencies (see instructions [here](https://gitlab.com/kimetrica/darpa/darpa)). Once the system is setup, in the terminal enter the Darpa root directory, execute `luigi --module <file_name> <task_name> --local-scheduler`

The crop model takes time to run. One can specify the sample size to be used to run the crop model as show below, though the total crop production will be incorrect. 
With a sample size of 5
`luigi --module models.crop_model.tasks models.crop_model.tasks.HarvestedYieldGeotiffs --models.crop-model.tasks.HarvestedYieldGeotiffs-sample 5 --local-scheduler`

For example, run maize production in Ethiopia use the command below:
`luigi --module models.crop_model.tasks models.crop_model.tasks.HarvestedYieldGeotiffs --models.crop-model.tasks.HarvestedYieldGeotiffs-crop maize --models.crop-model.tasks.HarvestedYieldGeotiffs-country-level Ethiopia --local-scheduler`

For example, run maize production in South Sudan use the command below:
`luigi --module models.crop_model.tasks models.crop_model.tasks.HarvestedYieldGeotiffs --models.crop-model.tasks.HarvestedYieldGeotiffs-crop maize --models.crop-model.tasks.HarvestedYieldGeotiffs-country-level 'South Sudan' --local-scheduler`

## Crop Model Output
The output of a crop model is total crop production. The task `HarvestedYieldGeotiffs` output raster file for each run (the default is 4 runs :- irrigated portion of crop, rain-fed high inputs portion of crop, rain-fed low inputs portion of crop and rain-fed subsistence portion of crop). The number of bands in each raster is equal to number of
run years the default number of years is 36. To retrive the band for each year used the python codel below
```
import rasterio
with rasterio.open(raster_file) as src:
    tags = src.tags()
print(tags)
```
<div align='center'><fig><img src="Geojson_output.png"  width="75%" height="75%"><figcaption>Fig.1. Crop model GeoJSON output. </figcaption></div>
<br>
<div align='center'><fig><img src="GeoTIFF_output.png"  width="75%" height="75%"><figcaption>Fig.2. Crop model GeoTIFF output. </figcaption></div>
<br>
