# Conflict Model 1: Baseline

# Introduction 

Armed conflicts that occur between one or more armed groups such as government or non-government forces are the causes of massive deaths and displacement in many countries. By the end of 2019, nearly 80 million people had been forcibly displaced worldwide due to armed conflict (Amnesty International, 2020). 

Appropriate early-warning systems that can meaningfully forecast the  conflict ints various forms is important to respond to the crisis ahead of time. This will have a significant impact on reducing the devastating consequence of conflict in terms of loss of lives, livelihoods, and infrastructure. The goal of the **conflict model** is to forecast armed conflict over time and space. The current status of the model runs forecasted values for the period between 07-2020 and 08-2021 for **Ethiopia** at admin2 or Zone level.

## Conflict trends in Ethiopia

A closer analysis of trends in the conflict in Ethiopia reveals that there is a spike in the frequency of conflict in recent years (Figure 1). On the other hand, there is an overall decline in the intensity of conflict (loss of lives per conflict event) (Figure 2). Perhaps, this might be due to a change in the nature of conflict from battles to other forms of conflict such as violence against civilians, riots and protests. 

*Figure 1: Conflict frequency*
![](readme_images/conflict_frequency.png)

*Figure 2: Conflict intensity (number of fatalities per conflict event)*
![](readme_images/conflict_intensity.png)

## Model Outputs
**Conflict Model 1: Baseline** is an initial attempt to forecast the onset of all types of armed conflict
on monthly basis and over one year period. Forecasted onset of conflict, 'cc_onset_y', is the output variable 
that indicates the absence (0) and presence (1) of conflict by month and at admin2 level. Current and historical data on armed conflict were used from ![ACLED](https://acleddata.com/#/dashboard) dataset. 


## Model Inputs

Below, the list of input features used in the model and their brief description are provided.

### Political and military (ACLED)
* `cc_onset_x`(t-1): Previous onset of conflict. 
* `fatalities`(t-1): Previous number of fatalities due to conflict. 
* `strategic_developments`(t-1): Strategic developments such as arrest of prominent persons or elections. 
* `cc_frequency`(t-1): Number of conflict events. 
* `actors`(t-i1: Actors involved in the conflict such as military, civilians, protesters, and rebel groups. 
* `event_type`(t-1): Type of conflict event such as protest, riots, and use of force. 
### Economic and Social (The 2007 Census and Population estimate)
* `TOT_POP`(t-1): Number of population. 
* `TOT_LIT`(2007): Percent of literate population. 
* `AGE_20-29 Percentage`(2007): Percent of youth population. 
* `religion_d`(2007): Religion diversity index. 
### Information and Infrastructure (The 2007 Census)
* `mobile_percentage`(2007): Percent of population who have access to cell phone.   
* `Television_percentage`(2007): Percent of population who have access to television. 
* `Radio_percentage`(2007): Percent of Population who have access to radio. 
### Physical Environment (WAPOR)
* `lc_d`(t-1): land cover diversification index. 
* `mean_rf`(t-1): monthly mean rainfall (mm). 

## Relationship between conflict/output and selected input variables 

*Figure 3: Relationship between 'conflict' and selected input variables*
![](readme_images/relationships.png)

# Model Description

## Functional form
The objective here is to estimate the standard functional form of: 
```math
ŷ_{i,t+1} = f(y_{i,t-k:t}, x_{i,t-k:t}, s_{i})
```
Where, 
```math
ŷ_{i,t+1}
``` 
is a binary output representing future onset of conflict at the subnational level (1, conflict; 0, no conflict); 
```math
y_{i,t-k:t}

``` 
is current and past onset of conflict;
```math
x_{i,t-k:t}

```  
are dynamic input variables in the current and past state; 
```math
s_{i}

``` 
are static input variables such as location reference; and 
f(.) is the prediction function learned by the model.

The conflict data has two distinct feature that requires special care during computation as compared to conventional machine learning problems. These are class imbalance and the issue of recurrence. 

## Class imbalance

Class imbalance refers to the disproportionate distribution of conflict and non-conflict events in the target variables. In this case, the number of conflict events tends to be much lower than the non-conflict events as conflict arises in a rare like situation on the ground (Figure 4). 

*Figure 4: Class imbalance-percent of 'conflict' records*
![](readme_images/class_imbalance.png)

Algorithms that take care of class imbalance usually outperform their counterparts.

## Recurrence

Recurrence, on the other hand, is a tendency of conflict to last for longer-period once it occurred (Figure 5). In the model, the problem of recurrence has been addressed through the use probability threshold to decide on the absence or presence of conflict based on the model output. This method entails that the model takes the past state of conflict in absence of strong evidence to decide upon the future situation.

*Figure 5: Probability of reocurrence of conflict relative to the current month*
![](readme_images/recurrence.png)

## Model Selection

We evaluated different forms of algorithms that include EasyEnsemble classifier, standard random forests classifier, logistic regression (classifier), xgb classifier, as well as Ada boost classifiers.

The model performance criteria is based a method called confusion matrix which shows the extent to which a model is correctly classifying the conflict(True positive) and no-conflict/peaceful (True negative) events in the data. Depending on the nature of the problem, the target is to obtain large proportion of either one or both of these values. For instance health screening problems such as cancer identification aim to obtain large number of true positive as much as possible to avoid the risk of missing actual cases. In doing so, the algorithms might run into having low proportion of true negative values. Similarly, in the case of conflict, it might be advantegeous to maximize the prediction of True negative values. As such, the best performing model is selected based on the number of conflict events classified correctly.

Accordingly, **EasyEnsemble Classifier** outperformed all other algorithms by having large number of True negative as well as a reasonable number of True positive (see Figure 6). This is because, among other things, **EasyEnsemble Classifier** handles the issue of class imbalance. In this case, the classifier samples several subsets from the majority class and combines them for a final decision. These independent samples ultimately take into account the different aspects of the entire dataset

*Figure 6: Confusion matrices*
![](readme_images/confusion_matrices.png)


# Model evaluation/prediction and forecasting

Actual vs predicted values of onset of conflict are compared in the model evaluation stage. This phase covers 12 months between August 2019 to July 2020. Figure 7 presents the overall comparison of actual and predicted values of conflict during the entire period. Accordingly, similar to the actual pattern of conflict frequency was obtained from the model output (Figure 7).  

*Figure 7: Frequency of Actual vs Predicted values of conflict*
![](readme_images/actual_vs_prediction.png)

## How to run model evaluation/prediction?

Model evaluation results can be viewd on monthly basis using the following command:

`luigi --module models.conflict_model.tasks models.conflict_model.tasks.Prediction --local-scheduler`

The output from the above command shows actual vs predicted values of conflict for each month during 2019/2020.

## How to run the model forecast?

To run the forecast use the command:
`luigi --module models.conflict_model.tasks models.conflict_model.tasks.Forecast --local-scheduler`

# Future importances 

To this end, the following figure presents the twenty most important features that predicted the onset of conflict in Ethiopia. Accordingly, mean rainfall ('mean_rf'), total number of population ('TOT_POP'), and land cover change index ('lc) are the most important contributer of the model.

*Figure 8: Twenty most important predictors of conflict*
![](readme_images/feature_importances.png)
