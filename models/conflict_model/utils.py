import numpy as np


def diversity_index(df, list_columns_tuple, var_name, include_year=False):
    list_columns = [i[1] for i in list_columns_tuple]
    if include_year:
        df = df[["admin1", "admin2", "year"] + list_columns].copy()
    else:
        df = df[["admin1", "admin2"] + list_columns].copy()
    for c in list_columns_tuple:
        df[c[0]] = df[c[1]] * np.log(df[c[1]]) * (1 / 100)
    sum_cols = [i[0] for i in list_columns_tuple]
    df[var_name] = df[sum_cols].sum(axis=1)
    df[var_name] = df[var_name] * (-1)
    return df
