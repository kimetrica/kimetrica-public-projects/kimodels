import logging

import geopandas as gpd
import numpy as np
import pandas as pd
from imblearn.ensemble import EasyEnsembleClassifier
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler
from xgboost import XGBClassifier

from kiluigi.targets import CkanTarget, FinalTarget, IntermediateTarget
from luigi import ExternalTask, Task
from luigi.util import requires

from .data_prep import TimeseriesShifting

logger = logging.getLogger("luigi-interface")


@requires(TimeseriesShifting)
class FeatureSelection(Task):
    def output(self):
        return IntermediateTarget(task=self, timeout=31536000)

    def run(self):
        with self.input().open() as src:
            df = src.read()
        drop_vars = [
            "country",
            "admin0",
            "month",
            "location",
            "admin1",
            "admin2",
            "year",
            "index",
            "Adm2_sqKm",
            "geometry",
        ]
        drop_vars = [i for i in drop_vars if i in df.columns]

        df2 = df.drop(drop_vars, axis=1)
        df2 = df2.dropna(subset=["cc_onset_y"])
        df2 = df2.dropna(axis=1)
        X = df2.drop(["cc_onset_y"], axis=1)
        y = df2.cc_onset_y
        model = Pipeline(
            [("StandardScaller", StandardScaler()), ("RF", ExtraTreesClassifier())]
        )
        model.fit(X, y)
        feat_importances = model.named_steps["RF"].feature_importances_
        most_important = dict(
            sorted(
                dict(zip(X.columns, feat_importances)).items(),
                key=lambda x: x[1],
                reverse=True,
            )
        )
        most_important = [k for k, v in most_important.items() if v > 0]
        with self.output().open("w") as out:
            out.write(most_important)


@requires(FeatureSelection, TimeseriesShifting)
class ModelTraing(Task):
    def output(self):
        return IntermediateTarget(task=self, timeout=31536000)

    def run(self):

        with self.input()[1].open() as src:
            df = src.read()
        with self.input()[0].open() as src:
            predictors_list = src.read()
        df3 = df.dropna(subset=["cc_onset_y"])

        end_date = "2019-08"
        mask = df3["month"] < end_date
        train1 = df3.loc[mask]

        start_date = "2019-07"
        end_date = "2020-08"
        mask = (df3["month"] > start_date) & (df3["month"] < end_date)
        test1 = df3.loc[mask]

        end_date = "2020-08"
        mask = df3["month"] < end_date
        re_train1 = df3.loc[mask]

        start_date = "2020-07"
        end_date = "2021-08"
        mask = (df["month"] > start_date) & (df["month"] < end_date)
        current = df.loc[mask]

        X_train = train1[predictors_list]
        X_test = test1[predictors_list]
        X_re_train = re_train1[predictors_list]
        y_train = train1.cc_onset_y
        y_test = test1.cc_onset_y
        y_re_train = re_train1.cc_onset_y

        X_current = current[predictors_list]
        # y_current = current.cc_onset_y

        estimator_ee = Pipeline(
            [
                ("StandardScaller", StandardScaler()),
                (
                    "RF",
                    EasyEnsembleClassifier(
                        n_estimators=15,
                        n_jobs=-1,
                        base_estimator=XGBClassifier(
                            max_delta_step=1,
                            base_estimator__gama=0.016237767391887217,
                            base_estimator__alpha=100,
                            base_estimator__max_delta_step=1,
                            base_score=0.23,
                        ),
                        sampling_strategy="auto",
                    ),
                ),
            ]
        )

        estimator_ee.fit(X_train, y_train)
        logger.info(f"The score is: {estimator_ee.score(X_test,y_test)}")

        with self.output().open("w") as out:
            out.write(
                {
                    "model": estimator_ee,
                    "X_test": X_test,
                    "y_test": y_test,
                    "X_re_train": X_re_train,
                    "y_re_train": y_re_train,
                    "X_current": X_current,
                    "current": current,
                    "test1": test1,
                }
            )


class PullAdminData(ExternalTask):
    def output(self):
        return CkanTarget(
            dataset={"id": "4dbc3cc7-9474-49f2-bfd4-231e78401caa"},
            resource={"id": "f50b0363-e3fd-4020-808e-3a26a62511bb"},
        )


@requires(PullAdminData)
class NormalizeAdminData(Task):
    def output(self):
        return IntermediateTarget(task=self, timeout=31536000)

    def run(self):
        admin = gpd.read_file(f"zip://{self.input().path}")
        # admin[["admin1", "admin2"]] = admin.location.str.split("_", expand=True,)
        admin = admin.rename(columns={"ADMIN1": "admin1", "ADMIN2": "admin2"})
        df_admins = admin[["admin1", "admin2", "Adm2_sqKm", "geometry"]]
        df_admins = df_admins.drop_duplicates()
        with self.output().open("w") as out:
            out.write(df_admins)


@requires(ModelTraing, NormalizeAdminData)
class Prediction(Task):
    def output(self):
        dst = "conflict_models/conflict_model_1_actual_vs_prediction.geojson"
        try:
            return FinalTarget(path=dst, task=self, ACL="public-read")
        except TypeError:
            return FinalTarget(path=dst, task=self)

    def run(self):
        with self.input()[1].open() as src:
            admin_df = src.read()
        with self.input()[0].open() as src:
            input_map = src.read()
        estimator_ee, X_test, y_test, test1 = (
            input_map["model"],
            input_map["X_test"],
            input_map["y_test"],
            input_map["test1"],
        )
        pred_prob = estimator_ee.predict_proba(X_test)

        prob_diff = []
        for prob in pred_prob:
            prob_diff.append(abs(prob[1] - prob[0]))

        prob_pred = []
        for prob, t in zip(pred_prob, y_test):
            if abs(prob[1] - prob[0]) > np.mean(prob_diff):
                prob_pred.append(np.argmax(prob))
            else:
                prob_pred.append(int(t))
        df_prob_pred = pd.DataFrame(prob_pred)
        df_prob_pred.rename(columns={0: "cc_onset_prediction"}, inplace=True)

        df_test = test1.reset_index()
        df_evl = df_test.join(df_prob_pred)

        df_evl["month"] = pd.to_datetime(df_evl.month)
        df_evl["month_year"] = df_evl["month"].dt.to_period("M").astype(str)
        df_evl["month_year"] = df_evl["month_year"]

        df_evl = df_evl[
            [
                "admin1",
                "admin2",
                "location",
                "month_year",
                "cc_onset_y",
                "cc_onset_prediction",
            ]
        ]
        df_evl = df_evl.merge(admin_df, on=["admin1", "admin2"], how="left")
        df_evl = gpd.GeoDataFrame(df_evl)
        df_evl["lon"] = df_evl.centroid.x
        df_evl["lat"] = df_evl.centroid.y
        with self.output().open("w") as out:
            df_evl.to_file(out.name, driver="GeoJSON")


@requires(ModelTraing)
class Retrain(Task):
    def output(self):
        return IntermediateTarget(task=self, timeout=31536000)

    def run(self):
        with self.input().open() as src:
            data_map = src.read()
        X_re_train, y_re_train = data_map["X_re_train"], data_map["y_re_train"]
        estimator_ee_rt = Pipeline(
            [
                ("StandardScaller", StandardScaler()),
                (
                    "RF",
                    EasyEnsembleClassifier(
                        n_estimators=15,
                        n_jobs=-1,
                        base_estimator=XGBClassifier(
                            max_delta_step=1,
                            base_estimator__gama=0.016237767391887217,
                            base_estimator__alpha=100,
                            base_estimator__max_delta_step=1,
                            base_score=0.23,
                        ),
                        sampling_strategy="auto",
                    ),
                ),
            ]
        )

        estimator_ee_rt.fit(X_re_train, y_re_train)
        logger.info(
            f"Re-train model score is: {estimator_ee_rt.score(X_re_train,y_re_train)}"
        )

        with self.output().open("w") as out:
            out.write(estimator_ee_rt)


@requires(Retrain, ModelTraing, NormalizeAdminData)
class Forecast(Task):
    def output(self):
        dst = "conflict_models/cc_conflict_model_1_forecast.geojson"
        try:
            return FinalTarget(path=dst, task=self, ACL="public-read")
        except TypeError:
            return FinalTarget(path=dst, task=self)

    def run(self):
        with self.input()[2].open() as src:
            df_admins = src.read()
        with self.input()[0].open() as src:
            estimator_ee_rt = src.read()
        with self.input()[1].open() as src:
            data_map = src.read()
            X_current, y_test, current = (
                data_map["X_current"],
                data_map["y_test"],
                data_map["current"],
            )

        y_forecast = estimator_ee_rt.predict_proba(X_current)
        prob_diff = []
        for prob in y_forecast:
            prob_diff.append(abs(prob[1] - prob[0]))

        prob_forecast = []
        for prob, t in zip(y_forecast, y_test):
            if abs(prob[1] - prob[0]) > np.mean(prob_diff):
                prob_forecast.append(np.argmax(prob))
            else:
                prob_forecast.append(int(t))

        df_forecast = pd.DataFrame(prob_forecast)
        df_forecast.rename(columns={0: "cc_onset_forecast"}, inplace=True)
        df_current = current.reset_index()
        df_forecast = df_current.join(df_forecast)
        df_forecast["month"] = pd.to_datetime(df_forecast.month)
        df_forecast["month"] = df_forecast["month"].dt.to_period("M")
        df_forecast["month"] = df_forecast["month"].astype(str)
        df_forecast.rename(columns={"month": "month_year"}, inplace=True)
        df_forecast = df_forecast[["month_year", "location", "cc_onset_forecast"]]
        df_forecast["month_year"] = df_forecast["month_year"].astype(str)
        cc_onset_forecast = df_forecast.pivot(
            index="location", columns="month_year", values="cc_onset_forecast"
        ).reset_index()
        cc_onset_forecast = df_forecast.pivot(
            index="location", columns="month_year", values="cc_onset_forecast"
        ).reset_index()
        cc_onset_forecast[["admin1", "admin2"]] = cc_onset_forecast.location.str.split(
            "_", expand=True,
        )
        cc_forecast = pd.merge(
            cc_onset_forecast, df_admins, how="left", on=["admin1", "admin2"]
        )

        cc_forecast.rename(
            columns={"admin1": "ADMIN1", "admin2": "ADMIN2"}, inplace=True
        )
        # cc_forecast1=pd.merge(gdf_eth, cc_forecast, how ='left', on = ['ADMIN1', 'ADMIN2'])

        col_names = [
            "2020-08",
            "2020-10",
            "2020-11",
            "2020-12",
            "2021-01",
            "2021-02",
            "2021-03",
            "2021-04",
            "2021-05",
            "2021-06",
            "2021-07",
        ]
        cc_forecast["2020/21"] = cc_forecast[col_names].sum(axis=1)
        cc_forecast = gpd.GeoDataFrame(cc_forecast)
        cc_forecast = cc_forecast.set_crs(epsg=4326)
        cc_forecast["lon"] = cc_forecast.centroid.x
        cc_forecast["lat"] = cc_forecast.centroid.y
        with self.output().open("w") as out:
            cc_forecast.to_file(out.name, driver="GeoJSON")
