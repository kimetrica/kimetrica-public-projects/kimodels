from functools import reduce

import pandas as pd
from impyute.imputation import cs
from sklearn.preprocessing import LabelBinarizer, MultiLabelBinarizer

from kiluigi.targets import CkanTarget, IntermediateTarget
from luigi import ExternalTask, Task

# from altair import datum
from luigi.util import requires

from .mapping import (
    acters_rename,
    admin_names,
    e_types_rename,
    ethnic_cols,
    impute_list,
    interaction_rename,
    land_cover_cols,
    religion_cols,
)
from .utils import diversity_index

# import panel as pn


class PullConflictData(ExternalTask):
    def output(self):
        return CkanTarget(
            dataset={"id": "4dbc3cc7-9474-49f2-bfd4-231e78401caa"},
            resource={"id": "1a878ecb-031c-4705-9986-0a37168e43d3"},
        )


@requires(PullConflictData)
class FeatureEngineering(Task):
    def output(self):
        return IntermediateTarget(task=self, timeout=31536000)

    def run(self):
        df = pd.read_csv(self.input().path)
        df["event_date"] = pd.to_datetime(df.event_date)

        # Interaction of actors
        one_hot = MultiLabelBinarizer()
        one_hot_1 = LabelBinarizer()

        df["interaction_1"] = df.interaction.astype(str)
        df["interaction_1"] = df.interaction_1.replace(interaction_rename)
        interaction = one_hot_1.fit_transform(df["interaction_1"].dropna())
        interaction = pd.DataFrame(interaction, columns=one_hot_1.classes_)

        # Event type
        e_types = one_hot.fit_transform(df["event_type"].dropna().str.split(", "))
        e_types = pd.DataFrame(e_types, columns=one_hot.classes_)
        e_types = e_types.rename(columns=e_types_rename)

        # Actors
        one_hot = MultiLabelBinarizer()
        acters = one_hot.fit_transform(
            df["interaction"].astype(str).dropna().str.split("")
        )
        acters = pd.DataFrame(acters, columns=one_hot.classes_)
        acters = acters.drop(["", "0"], axis=1)
        acters = acters.rename(columns=acters_rename)

        # Others( fatalities, lat and long)

        others = df[
            [
                "admin1",
                "admin2",
                "event_date",
                "fatalities",
                "latitude",
                "longitude",
                "year",
            ]
        ]
        others["fatalities"] = others.fatalities.astype(int)

        # Frequency of conflict
        others["cc_frequency"] = 1

        # longitude and latitude
        others["latitude"] = others.latitude.astype(float)
        others["longitude"] = others.longitude.astype(float)

        # merge conflict dfs
        conflict_dfs = [interaction, acters, e_types, others]
        conflict_merged = reduce(
            lambda left, right: pd.merge(
                left, right, left_index=True, right_index=True, how="outer"
            ),
            conflict_dfs,
        )
        with self.output().open("w") as out:
            out.write(conflict_merged)


# Aggregate
@requires(FeatureEngineering)
class AggregateConflictData(Task):
    def output(self):
        return IntermediateTarget(task=self, timeout=31536000)

    def run(self):
        with self.input().open("r") as src:
            df = src.read()
        df1 = df[["admin1", "admin2", "event_date", "cc_frequency", "fatalities"]]
        df2 = df.drop(["cc_frequency", "fatalities", "longitude", "latitude"], axis=1)
        df3 = df[["admin1", "admin2", "event_date", "longitude", "latitude"]]

        # group by admin and month#
        df1 = df1.groupby(["admin1", "admin2", "event_date"]).sum().reset_index()
        df2 = df2.groupby(["admin1", "admin2", "event_date"]).sum().reset_index()
        df3 = (
            df3.groupby(["admin1", "admin2", "event_date"])["latitude", "longitude"]
            .mean()
            .reset_index()
        )

        df2.set_index(
            ["admin1", "admin2", "event_date"], inplace=True,
        )
        df2[df2 > 0] = 1
        df2 = df2.reset_index()
        dfs = [df1, df2, df3]
        merged = reduce(
            lambda left, right: pd.merge(
                left, right, on=["admin1", "admin2", "event_date"], how="outer"
            ),
            dfs,
        )

        # Create Onset of conflict

        merged["cc_onset"] = 1

        # Account for peaceful months/ no conflict#
        filled_df = (
            merged.set_index("event_date")
            .groupby(["admin1", "admin2"])
            .apply(
                lambda d: d.reindex(
                    pd.date_range(
                        min(merged.event_date), max(merged.event_date), freq="D"
                    )
                )
            )
            .drop(["admin1", "admin2"], axis=1)
            .reset_index(["admin1", "admin2"])
            .fillna(0)
        )

        filled_df = filled_df.reset_index()
        filled_df["month_year"] = pd.to_datetime(filled_df["index"]).dt.to_period("M")
        filled_df["year"] = pd.to_datetime(filled_df["index"]).dt.to_period("Y")

        """
      #updateconflict onset#
        mask = f_df.cc_onset > 0
        column_name = 'cc_onset'
        f_df.loc[mask, column_name] = 1
        """

        with self.output().open("w") as out:
            out.write(filled_df)


@requires(AggregateConflictData)
class NormalizeAdminNames(Task):
    """
    Normalize the admin names
    """

    def output(self):
        return IntermediateTarget(task=self, timeout=31536000)

    def run(self):
        with self.input().open() as src:
            filled_df = src.read()

        f_df = filled_df.replace(admin_names)

        # Aggregate by admin2 and month"
        df_final = (
            f_df.groupby(["year", "month_year", "admin1", "admin2", "cc_onset"])
            .sum()
            .reset_index()
        )

        with self.output().open("w") as out:
            out.write(df_final)


class PullCensusData(ExternalTask):
    def output(self):
        return CkanTarget(
            dataset={"id": "4dbc3cc7-9474-49f2-bfd4-231e78401caa"},
            resource={"id": "fce5c598-5c2e-41f9-976b-25160aec667a"},
        )


class PullPopulationEstimates(ExternalTask):
    def output(self):
        return CkanTarget(
            dataset={"id": "4dbc3cc7-9474-49f2-bfd4-231e78401caa"},
            resource={"id": "feb65605-e033-4cd6-90ab-85a818ab8bd3"},
        )


@requires(PullCensusData, PullPopulationEstimates, NormalizeAdminNames)
class DiversityIndex(Task):
    def output(self):
        return IntermediateTarget(task=self, timeout=31536000)

    def run(self):
        df = pd.read_csv(self.input()[0].path)
        # Calculate religion diversity

        rel_d = diversity_index(df, religion_cols, "religion_d")

        # Ethnic diversity index

        eth_d = diversity_index(df, ethnic_cols, "eth_d")

        other = df[
            [
                "admin1",
                "admin2",
                "AGE_10-19 Percentage",
                "AGE_20-29 Percentage",
                "Radio_Percentage",
                "Phone_Percentage",
                "Television_Percentage",
                "TOT_LIT_Percentage",
            ]
        ]
        pop_est = pd.read_csv(self.input()[1].path)
        pop_dfs = [pop_est, rel_d, eth_d, other]

        pop_final = reduce(
            lambda left, right: pd.merge(
                left, right, how="outer", on=["admin1", "admin2"]
            ),
            pop_dfs,
        )

        with self.input()[2].open() as src:
            final = src.read()
        final["year"] = final["year"].astype(str)
        pop_final["year"] = pop_final["year"].astype(str)
        df_final = pd.merge(
            final, pop_final, how="left", on=["admin1", "admin2", "year"]
        )
        with self.output().open("w") as out:
            out.write(df_final)


class PullLandCover(ExternalTask):
    def output(self):
        return CkanTarget(
            dataset={"id": "4dbc3cc7-9474-49f2-bfd4-231e78401caa"},
            resource={"id": "c482db72-6005-4484-a205-270b5b7b64d5"},
        )


@requires(PullLandCover, DiversityIndex)
class CalculateLandCoverDiversity(Task):
    def output(self):
        return IntermediateTarget(task=self, timeout=31536000)

    def run(self):
        df = pd.read_csv(self.input()[0].path)

        df = df.rename(columns={"ADMIN1": "admin1", "ADMIN2": "admin2"})
        df = diversity_index(df, land_cover_cols, "lc", True)

        df = df[["admin1", "admin2", "year", "lc"]]

        df["year"] = df.year.astype(str)

        with self.input()[1].open() as src:
            df_final = src.read()

        df_final = pd.merge(df_final, df, how="left", on=["admin1", "admin2", "year"])

        with self.output().open("w") as out:
            out.write(df_final)


class PullRainfall(ExternalTask):
    def output(self):
        return CkanTarget(
            dataset={"id": "4dbc3cc7-9474-49f2-bfd4-231e78401caa"},
            resource={"id": "842ee7fa-4011-4d17-9c7c-be7e5eafca73"},
        )


@requires(PullRainfall, CalculateLandCoverDiversity)
class AddRainfallData(Task):
    def output(self):
        return IntermediateTarget(task=self, timeout=31536000)

    def run(self):
        rain_df = pd.read_csv(self.input()[0].path)

        with self.input()[1].open() as src:
            df_final = src.read()
        df_final = df_final.rename(columns={"month_year": "month"})
        df_final["month"] = df_final["month"].astype(str)
        df_final = pd.merge(
            df_final, rain_df, how="left", on=["admin1", "admin2", "month"]
        )
        df_final["location"] = (
            df_final["admin1"].astype(str) + "_" + df_final["admin2"].astype(str)
        )

        #  Impute missing data using multiple imputation method (mice)
        mice_impute = cs.mice(df_final[impute_list].values)

        df_mice = pd.DataFrame(
            mice_impute, columns=df_final[impute_list].columns, index=df_final.index
        )
        df_final[impute_list] = df_mice

        with self.output().open("w") as out:
            out.write(df_final)


@requires(AddRainfallData)
class TimeseriesShifting(Task):
    def output(self):
        return IntermediateTarget(task=self, timeout=31536000)

    def run(self):
        with self.input().open() as src:
            before_shifted = src.read()

        before_shifted = before_shifted.sort_values("cc_onset")
        before_shifted = before_shifted.drop_duplicates(
            subset=["month", "location"], keep="last"
        )
        target = before_shifted[["location", "month", "cc_onset"]]
        df = target.set_index(["month", "location"])
        df = df.unstack().shift(-12)
        df = df.stack(dropna=False)
        result = pd.merge(before_shifted, df, how="outer", on=["location", "month"])
        month_replace = {str(i): str(i + 1) for i in range(1997, 2021)}
        month_replace_new = {
            f"{k}-{str(i).zfill(2)}": f"{v}-{str(i).zfill(2)}"
            for k, v in month_replace.items()
            for i in range(1, 13)
        }

        result["month"] = result["month"].replace(month_replace_new)

        with self.output().open("w") as out:
            out.write(result)
